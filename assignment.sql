-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 19:00 PM
-- Server-versjon: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `id` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `city` varchar(32) NOT NULL,
  `county` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `distance`
--

CREATE TABLE `distance` (
  `sFallYear` year(4) NOT NULL,
  `skierUserName` varchar(32) NOT NULL,
  `totDist` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `seasonFallYear` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(32) NOT NULL,
  `firstName` varchar(32) NOT NULL,
  `lastName` varchar(32) NOT NULL,
  `yearOfBirth` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skis`
--

CREATE TABLE `skis` (
  `sFallYear` year(4) NOT NULL,
  `skierUserName` varchar(32) NOT NULL,
  `clubId` varchar(32) NOT NULL,
  `sDate` date NOT NULL,
  `sArea` varchar(60) NOT NULL,
  `sDistance` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distance`
--
ALTER TABLE `distance`
  ADD PRIMARY KEY (`sFallYear`,`skierUserName`),
  ADD KEY `skierUserName` (`skierUserName`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`seasonFallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skis`
--
ALTER TABLE `skis`
  ADD PRIMARY KEY (`sFallYear`,`skierUserName`,`clubId`),
  ADD KEY `skierUserName` (`skierUserName`),
  ADD KEY `clubId` (`clubId`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `distance`
--
ALTER TABLE `distance`
  ADD CONSTRAINT `distance_ibfk_1` FOREIGN KEY (`sFallYear`) REFERENCES `season` (`seasonFallYear`),
  ADD CONSTRAINT `distance_ibfk_2` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`);

--
-- Begrensninger for tabell `skis`
--
ALTER TABLE `skis`
  ADD CONSTRAINT `skis_ibfk_1` FOREIGN KEY (`sFallYear`) REFERENCES `season` (`seasonFallYear`),
  ADD CONSTRAINT `skis_ibfk_2` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `skis_ibfk_3` FOREIGN KEY (`clubId`) REFERENCES `club` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
