<?php

include_once("Model1.php");
include_once("View.php");


class Controller {
	public
		$db = null,
		$xml = null,
		$view = null;
	public function __construct() {  
		$this->db = new DBModel();					// Creating an object working on DB
		$this->view = new View();					// Creating an object for the view
	}
	public function invoke() {
		$this->db->Skiers();	// Retreiving skiers from XML
		$this->db->Clubs();	// Retreiving clubs from XML
		$this->db->Seasons();// Retreiving seasons from XML
		$this->db->Skis();
		$this->db->totDistance();
	}

}


?>
